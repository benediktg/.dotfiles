execute pathogen#infect()
set nocompatible "[!!]
set encoding=utf-8
set ffs=unix
set history=100
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.png,.jpeg,.jpg,.gif
set backspace=2 "[!!]
set number
set showmatch
set undofile "[!!]
set undodir=~/.vim/vimun "[!!]
set laststatus=2 "[!!]
set mouse=a
set tabstop=4 "[!!]
set shiftwidth=4 "[!!]
set expandtab "[!!]
set smarttab "[!!]
set autoindent "[!!]
set showcmd
set title
"set textwidth=80 "[!!]
set wrap "[!!]
set linebreak "[!!]
set formatoptions=tc,q,r
set foldmethod=expr
set foldcolumn=2
set hlsearch "[!!]
set incsearch
set ignorecase
set smartcase
set ruler
set cursorline
inoremap jk <ESC>
let mapleader = ","
map <F5> :setlocal spell! spelllang=de_de<CR>
syntax on
filetype plugin indent on

set background=dark
colorscheme solarized
set t_Co=16
