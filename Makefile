DIR=~/.dotfiles
OLDDIR=~/.old.dotfiles
FILES=zshrc vimrc gitconfig gitignore_global zprofile profile emacs.d
FILES2=bashrc gitconfig vimpagerrc oh-my-zsh $(FILES)
B=0

default: link

prepare:
	test -d $(OLDDIR) || mkdir -p $(OLDDIR)
	test -d ~/.cache/zsh || mkdir -p ~/.cache/zsh

link: prepare
	for file in $(FILES); do \
		test $(B) != 0 && mv ~/.$$file $(OLDDIR); \
		ln -s $(DIR)/$$file ~/.$$file; \
	done

clean:
	for file in $(FILES); do \
		test -L ~/.$$file && rm ~/.$$file; \
	done

remove:
	rm -r $(OLDDIR)
