dotfiles
========

Voraussetzungen:  
*vim*, *git*, *emacs* und *zsh* installiert

Konfiguration
-------------

* klonen und in das neue Verzeichnis wechseln

* `./make.sh`

* `mkdir -p ~/.vim/autoload ~/.vim/bundle/vim-colors-solarized`

* `curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim`

* im Terminalemulator Solarized Farbschema einstellen

* `cd ~/.vim/bundle`

* `git clone git://github.com/altercation/vim-colors-solarized.git`