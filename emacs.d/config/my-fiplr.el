(use-package fiplr
  :commands (fiplr-mode)
  :ensure fiplr
  :demand fiplr
  :init)

(setq fiplr-root-markers '(".git" ".svn"))
(setq fiplr-ignored-globs '((directories (".git" ".svn"))
                            (files ("*.jpg" "*.png" "*.zip" "*~"))))
(global-set-key (kbd "C-x f") 'fiplr-find-file)

(provide 'my-fiplr)
