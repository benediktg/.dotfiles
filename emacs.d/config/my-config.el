;; no startup messages and no toolbar
(setq inhibit-splash-screen t
      inhibit-startup-echo-area-message t
      inhibit-startup-message t)
(tool-bar-mode -1)

;; default font with 10 points
(set-default-font "DejaVu Sans Mono 10")

;; better line wrapping and line numbering
(visual-line-mode 1)
(global-linum-mode 1)
(column-number-mode)

;; auto indenting
(define-key global-map (kbd "RET") 'newline-and-indent)

;; theme
(load-theme 'adwaita)
;(load-theme 'solarized-light)

;; 'y' and 'n' instead of 'yes' and 'no'
(defalias 'yes-or-no-p 'y-or-n-p)

;; Wrap words at their boundaries, not anywhere else:
(setq-default word-wrap t)

;; Highlight matching parens:
(show-paren-mode t)
(setq show-paren-delay 0) ; no delay, damnit!

;; Enable the mouse in terminal mode.
(xterm-mouse-mode 1)

;; UTF-8 everything!
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; This isn't a typewriter (even if it is a terminal); one space after sentences,
;; please.
(setq sentence-end-double-space nil)

;; Flash the frame to represent a bell.
(setq visible-bell t)
;; nevermind that's annoying
(setq ring-bell-function 'ignore)

;; The default of 16 is too low. Give me a 64-object mark ring.
;; Across all files, make it 128.
(setq mark-ring-max 64)
(setq global-mark-ring-max 128)

;; Display the current function name in the modeline.
(which-function-mode 0)

;; lockfiles are evil.
(setq create-lockfiles nil)

;; name the title based on the file
(defun my-update-emacs-title ()
  "Update the Emacs title based on the current buffer.
If the current buffer is associated with a filename, that filename will be
used to tile the window. Otherwise, the window will be titled based upon the
name of the buffer."
  (if (buffer-file-name (current-buffer))
      (setq frame-title-format "Emacs - %f")
    (setq frame-title-format "Emacs - %b")))
(cl-dolist (hook '(buffer-list-update-hook
		   change-major-mode-hook
		   find-file-hook))
  (add-hook hook 'my-update-emacs-title))

;; after macro
(defmacro after (mode &rest body)
  "'eval-after-load' MODE evaluate BODY."
  (declare (indent defun))
  '(eval-after-load 'mode
     '(progn '@body)))

(provide 'my-config)
