(use-package tex-site
  :ensure auctex
  :config
  (progn
    ;(load "auctex.el" nil t t)
    ;(load "preview-latex.el" nil t t)
    (setq TeX-auto-save t)
    (setq TeX-parse-self t)
    (setq TeX-PDF-mode t)
    (setq-default TeX-master nil)
    (add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (TeX-fold-mode 1)))
    (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
    (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
    (add-to-list 'auto-mode-alist '("\\.latex" . tex-mode))

    (use-package auctex-latexmk
      :ensure auctex-latexmk
      :init
      (progn
	(auctex-latexmk-setup)
	)
      )
    )
  )



(provide 'lang-latex2)
