(use-package elisp-slime-nav
  :ensure elisp-slime-nav
  :init
  (progn
    (defun my-lisp-hook ()
      (progn
	(elisp-slime-nav-mode)
	(turn-on-eldoc-mode)))
    (add-hook 'emacs-lisp-mode-hook 'my-lisp-hook))
  :config
  ;(progn
  ;  (after 'evil
  ;	   (evil-define-key 'normal emacs-lisp-mode-map (kbd "K")
  ;	     'elisp-slime-nav-describe-elisp-thing-at-point))))
)
(provide 'my-elisp)
