(setq evil-emacs-state-cursor '("red" box))
(setq evil-normal-state-cursor '("green" box))
(setq evil-visual-state-cursor '("orange" box))
(setq evil-insert-state-cursor '("red" bar))
(setq evil-replace-state-cursor '("red" bar))
(setq evil-operator-state-cursor '("red" hollow))

(use-package evil-leader
      :commands (evil-leader-mode)
      :ensure evil-leader
      :demand evil-leader
      :init
      (global-evil-leader-mode)
      :config
      (progn
        (evil-leader/set-leader ",")
	(evil-leader/set-key "w" 'save-buffer)
	(evil-leader/set-key "q" 'kill-buffer-and-window)

	(evil-leader/set-key "h" 'dired-jump)
	(evil-leader/set-key "v" 'split-window-right)
	(evil-leader/set-key "e" 'pp-eval-last-sexp)
	(evil-leader/set-key "," 'other-window)
	(evil-leader/set-key "b" 'ibuffer)
	(evil-leader/set-key "o" 'org-mode)
	(evil-leader/set-key "s" 'eshell)
	(evil-leader/set-key "x" 'helm-M-x)))

(use-package evil
  :ensure evil)

(provide 'my-evil-leader)
