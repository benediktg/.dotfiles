(use-package guide-key
  :ensure guide-key
  :config
  (progn
    (setq guide-key/guide-key-sequence '("C-x r" "C-x 4"))
    (guide-key-mode 1)))  ; Enable guide-key-mode

(provide 'my-guide-key)
