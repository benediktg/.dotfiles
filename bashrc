#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# prompt colors
reset=$(tput sgr0)

black=$(tput setaf 0)
bgblack=$(tput setab 0)
red=$(tput setaf 1)
bgred=$(tput setab 1)
green=$(tput setaf 2)
bggreen=$(tput setab 2)
yellow=$(tput setaf 3)
bgyellow=$(tput setab 3)
blue=$(tput setaf 4)
bgblue=$(tput setab 4)
magenta=$(tput setaf 5)
bgmagenta=$(tput setab 5)
cyan=$(tput setaf 6)
bgcyan=$(tput setab 6)
white=$(tput setaf 7)
bgwhite=$(tput setab 7)

bold=$(tput bold) # bold text
hbright=$(tput dim) # half-bright
uline=$(tput smul) # underlined
nouline=$(tput rmul) # stop underlined
reverse=$(tput rev)
standout=$(tput smso)
nostandout=$(tput rmso)

#PS1='\[$red\]\u\[$reset\] \[$blue\]\w\[$reset\] \[$red\]\$ \[$reset\]\[$green\] '
PS1='\[$red\]\[$bold\]\u\[$reset\] \[$blue\]\W\[$reset\] \[$bold\]\[$red\]\$ \[$reset\] '

#PS1='[\u@\h \W]\$ '
export PROMPT_COMMAND='echo -ne "\033]0;$PWD\007"'


#eval 'dircolors ~/.dir_colors'

export EDITOR=vim
alias ls='ls --color=auto'
alias g='gvim --remote-silent'

# Macht die Ausgabe von ls farbig:
alias ls='ls --color=auto' 
 
# Gibt zu jeder Datei weitere Informationen aus. Größenangaben immer
# "menschenlich lesbar":
alias ll='ls -lh' 
 
# Wie ll, aber es werden auch versteckte Dateien aufgelistet:
alias la='ls -lha' 
 
# Für Vertipper recht praktisch:
alias cd..='cd ..' 
alias cd-='cd -'
 
# Abkürzung für cd .. :
alias ..='cd ..' 
alias ...='cd ../..'

# Bei df die Größen immer "menschlich lesbar" anzeigen:
alias df='df -h' 
 
# Bei du die Größen immer "menschlich lesbar" anzeigen:
alias du='du -h' 
 
# Suche in less case-insensitive:
alias l='less -i' 
 
# Zeigt Änderungen einer Datei an (wie tail -f):
alias lf='less -i +F' 
 
# Beispielhafte Abkürzung für ssh
alias meinserver='ssh user@meinserver.tld'
 
# lässt grep case-insensitive arbeiten und markiert die Treffer farbig:
alias grep='grep -i --color=auto'

# Starte Kommando in neuem Screen-Fenster:
alias s='screen -X screen' 
 
# Dateinamen im aktuellen Verzeichnis nach Ausdruck durchsuchen:
alias lg='ls --color=always | grep --color=always -i' 
 
# Timestamp generieren:
alias timestamp='date "+%Y%m%d%H%M%S"' 
 
# Verzeichnis mit aktuellem Timestamp erstellen:
alias tsdir='timestamp | xargs mkdir' 
 
# Änderungen bei allen angegebenen Logdateien live anzeigen:
alias mylogs='sudo tail -f /var/log/{syslog,messages}' 
 
# Einen Prozess (nach Namen) suchen:
alias psg='ps ax | grep -v grep | grep ' 
 
# Die öffentliche IP ausgeben:
alias myip="wget -qO - http://checkip.dyndns.org | sed 's/[a-zA-Z<>/ :]//g'" 
 
# Zuletzt bearbeitete Datei in vim öffnen:
alias lvim="vim -c \"normal '0\"" 
 
# Die 10 Prozesse anzeigen, die den meisten RAM verbrauchen:
alias psram='ps aux | sort -rnk 4 | head'
 
# Zeige alle Prozesse bei ps (außer ps selbst) mit langer Ausgabe
alias ps='ps ax | grep -v "ps ax"'

# Pacman
alias pSyu='sudo pacman -Syu'
alias pS='sudo pacman -S'

# Apt
alias agu='sudo apt-get update && sudo apt-get upgrade'
alias agdu='sudo apt-get update && sudo apt-get dist-upgrade'
alias agi='sudo apt-get install'

alias nautilus='nautilus -w'
alias files='nautilus -w ./'

# Trackpoint
. ~/.dotfiles/trackpoint.sh
